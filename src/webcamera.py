#!/usr/bin/env python3

from datetime import datetime
import RPi.GPIO as GPIO
import subprocess
import time
import requests
import io
import os

token = ''
path = '/home/pi/picamera/resend'
prefix = ''

device = '/dev/video0'

width = 1280
height = 720

GPIO_SHUTTER = False
GPIO_PIN_SHUTTER = 26

headers = {
    'Authorization': 'Bearer ' + token
}

data = {
    'path': 'source'
}

timeout = 5
verify = True

prefix = '' if prefix == '' else prefix + '-'

GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_PIN_SHUTTER, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)

while True:
    is_pull_up = bool(GPIO.input(GPIO_PIN_SHUTTER) == GPIO.HIGH) if GPIO_SHUTTER else False

    if bool(not GPIO_SHUTTER) ^ bool(is_pull_up):
        process = subprocess.Popen(
            ['fswebcam', '--device', f'{device}', '--resolution', f'{width}x{height}', '--skip', '25', '--jpeg', '100', '--no-banner', '-'],
            stdout = subprocess.PIPE
        )

        time.sleep(3)

        image = io.BytesIO()
        image.write(process.stdout.read())

        file_name = "{name}.{extension}".format(
            name = prefix + datetime.utcnow().strftime("%Y-%m-%d_%H%M%S%f")[:-3],
            extension = 'jpeg',
        )

        files = { 'file': ( file_name, image.getvalue() ) }

        try:
            _ = requests.post('https://00x56.com/api/v1/files', headers = headers, data = data, files = files, timeout = timeout, verify = verify)
        except Exception as err:
            os.makedirs(path, exist_ok = True)
            with open(os.path.join(path, file_name), 'wb') as f:
                f.write(image.getvalue())
        finally:
            time.sleep(1)

GPIO.cleanup()
