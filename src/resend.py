#!/usr/bin/env python3

import os
import requests

token = ''
path = '/home/pi/picamera/resend'

def is_internet(url = 'https://google.com', timeout = 5):
    try:
        _ = requests.head(url, timeout = timeout)

        return True
    except requests.exceptions.ConnectionError as errc:
        print('Error Connecting:', errc)

    return False

headers = {
    'Authorization': 'Bearer ' + token
}

data = {
    'path': 'source'
}

timeout = 10
verify = True

os.makedirs(path, exist_ok = True)

while True:
    if is_internet():
        for f in os.listdir(path):
            file_name = os.path.join(path, f)
            if os.path.isfile(file_name):
                try:
                    files = { 'file': open(file_name, 'rb') }
                    _ = requests.post('https://00x56.com/api/v1/files', headers = headers, data = data, files = files, timeout = timeout, verify = verify)

                    os.remove(file_name)
                except requests.exceptions.RequestException as err:
                    print ('OOps:', err)
                except requests.exceptions.HTTPError as errh:
                    print ('Http Error:', errh)
                except requests.exceptions.Timeout as errt:
                    print('Timeout Error:', errt)
                except requests.exceptions.ConnectionError as errc:
                    print('Error Connecting:', errc)
