#!/usr/bin/env python3

from picamera2 import Picamera2
from libcamera import Transform
from datetime import datetime
import RPi.GPIO as GPIO
import time
import requests
import io
import os

token = ''
path = '/home/pi/picamera/resend'
prefix = ''

width = 1280
height = 720

GPIO_SHUTTER = False
GPIO_PIN_SHUTTER = 26

headers = {
    'Authorization': 'Bearer ' + token
}

data = {
    'path': 'source'
}

timeout = 5
verify = True

prefix = '' if prefix == '' else prefix + '-'

GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_PIN_SHUTTER, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)

while True:
    is_pull_up = bool(GPIO.input(GPIO_PIN_SHUTTER) == GPIO.HIGH) if GPIO_SHUTTER else False

    if bool(not GPIO_SHUTTER) ^ bool(is_pull_up):
        picam2 = Picamera2()

        preview_config = picam2.create_preview_configuration(main = { 'size': (width, height) })
        # preview_config = picam2.create_preview_configuration(main = { 'size': (width, height) }, transform = Transform(180))
        picam2.configure(preview_config)

        picam2.start()

        time.sleep(3)

        image = io.BytesIO()
        picam2.capture_file(image, format = 'jpeg')

        file_name = "{name}.{extension}".format(
            name = prefix + datetime.utcnow().strftime("%Y-%m-%d_%H%M%S%f")[:-3],
            extension = 'jpeg',
        )

        files = { 'file': ( file_name, image.getvalue() ) }

        try:
            _ = requests.post('https://00x56.com/api/v1/files', headers = headers, data = data, files = files, timeout = timeout, verify = verify)
        except Exception as err:
            os.makedirs(path, exist_ok = True)
            with open(os.path.join(path, file_name), 'wb') as f:
                f.write(image.getvalue())
        finally:
            picam2.close()

            time.sleep(1)

GPIO.cleanup()
