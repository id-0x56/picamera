#!/bin/bash

Help() {
    echo "Usage: $0 [options...]"
    echo " -h   Print this Help"
    echo " -t   Set access token [required]"
    echo " -p   Set path to save [recommend]"
    echo " -d   Delete remote files"

    exit
}

path=$(pwd)/remote
token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSm9obiBEb2UifQ.DjwRE2jZhren2Wt37t5hlVru6Myq4AhpGLiiefF69u8"
is_delete=false

while getopts "ht:p:d" option; do
    case $option in
        h) Help;;
        t) token=$OPTARG;;
        p) path=$OPTARG;;
        d) is_delete=true;;
    esac
done

files=$(curl --silent --show-error --insecure \
    -X GET https://00x56.com/api/v1/files \
    -H "Authorization: Bearer $token" \
    -H "Accept: application/json" \
        | $(pwd)/bin/jq-linux-$(dpkg --print-architecture) -r ".[] | .name")

path=$(echo "$path" | sed 's:/*$::')
for name in $files; do
    file_path=${name%/*}
    file_name=${name##*/}

    echo $path$file_path/$file_name
    curl --show-error --insecure --progress-bar \
        -X GET https://00x56.com/api/v1/files$file_path/obj/$file_name \
        -H "Authorization: Bearer $token" \
        --output-dir $path$file_path --create-dirs \
        --remote-name
    echo

    if [ $is_delete = true ]; then
        curl --silent --show-error --insecure \
            -X DELETE https://00x56.com/api/v1/files$file_path/obj/$file_name \
            -H "Authorization: Bearer $token"
    fi
done
