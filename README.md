# Camera
### Wiring schema for photo by switch (optional)
![localImage](./docs/images/raspberry.png)
### Installation
```
sudo apt update \
    && sudo apt install -y git curl v4l-utils fswebcam
```
### Clone application
```
git clone https://gitlab.com/id-0x56/picamera.git
```
```
cd picamera \
    && chmod -R +x $(pwd)/bin
```
### Configure application
```
TOKEN=
```
```
sed -i "s|token = ''|token = '$TOKEN'|g" $(pwd)/src/*.py
```
### Create camera service for csi camera
```
sudo tee /lib/systemd/system/camera.service > /dev/null <<- EOF
[Unit]
After=network.target

[Service]
Type=idle
Restart=on-failure
User=root
ExecStart=/usr/bin/python3 $(pwd)/src/picamera.py

[Install]
WantedBy=multi-user.target
EOF
```
### Edit camera service for web camera
```
sudo sed -i "s|picamera.py|webcamera.py|g" /lib/systemd/system/camera.service
```
### Create resend service
```
sudo tee /lib/systemd/system/resend.service > /dev/null <<- EOF
[Unit]
After=network.target

[Service]
Type=idle
Restart=on-failure
User=root
ExecStart=/usr/bin/python3 $(pwd)/src/resend.py

[Install]
WantedBy=multi-user.target
EOF
```
### Enable services
```
sudo systemctl daemon-reload \
    && sudo systemctl enable camera.service --now \
    && sudo systemctl enable resend.service --now
```
### Restart services
```
sudo systemctl daemon-reload \
    && sudo systemctl restart camera.service \
    && sudo systemctl restart resend.service
```
# bash
### Download files, optional delete remote files
```
Usage: ./bin/dl.sh [options...]
 -h   Print this Help
 -t   Set access token [required]
 -p   Set path to save [recommend]
 -d   Delete remote files
```
```
./bin/dl.sh -t eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSm9obiBEb2UifQ.DjwRE2jZhren2Wt37t5hlVru6Myq4AhpGLiiefF69u8
```
